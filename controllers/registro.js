const { query } = require('express')
const { response } = require('express')
    //const sql = require('mssql')

const { Registro } = require('../models/Registro')
    //const { insertQuery, selectQuery } = require('../helpers/crud')

const getRegistros = async(req, res = response) => {

    try{
        const registros = await Registro.findAll()

        //const { query } = req

        res.json(registros)

    }catch (error) {
        res.status(404).json({
            msg: error.toString(),
            query
        })
    }
}

const createRegistro = async(req, res = response) => {

    const { query } = req

    try {

        const registro = await Registro.create(query)

        res.json(registro)
    } catch (error) {
        res.status(404).json({
            msg: error.toString(),
            query
        })
    }
}

const deleteRegistro = async(req, res = response) => {
    const { ids } = req.query
    try {
        await Registro.destroy({
            where: { id: JSON.parse(ids) }
        })
        res.json({
            msg: 'Eliminacion exitosa',
        })
    } catch (error) {
        res.status(404).json({
            msg: error.toString()
        })
    }
}

const updateRegistro = async(req, res = response) => {
    const { query } = req
    try {
        await Registro.update(query, {
            where: { id: query.id }
        })
        res.json({
            msg: 'Actualizacion exitosa'
        })
    } catch (error) {
        res.status(404).json({
            msg: error.toString()
        })
    }
}

module.exports = {
    createRegistro,
    getRegistros,
    deleteRegistro,
    updateRegistro
}