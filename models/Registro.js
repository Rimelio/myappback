const { DataTypes } = require('sequelize')
const { db } = require('../database/config')

const Registro = db.define('registro', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING
    },
    age: {
        type: DataTypes.INTEGER
    },
    birth: {
        type: DataTypes.DATEONLY
    },
    subscription: {
        type: DataTypes.DATEONLY
    },
    cost: {
        type: DataTypes.STRING
    }
}, {
    timestamps: false,
    freezeTableName: true
})

module.exports = {
    Registro
}