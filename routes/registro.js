const { Router } = require('express');
const { check } = require('express-validator');

//importando funciones//
const { getRegistros, createRegistro, deleteRegistro, updateRegistro } = require('../controllers/registro');

const router = Router();

router.get('/', getRegistros)

router.post('/', createRegistro)

router.delete('/', deleteRegistro)

router.put('/', updateRegistro)

module.exports = router;